-- Find alla rtist that has letter d in its name
SELECT *
FROM artists
WHERE name like "%d%";

-- FInd all songs that has a length of less than 230
SELECT *
FROM songs
WHERE length < 230;

-- JOIN the albums and songs tables.(Only show the album name, song name, and song length)
SELECT albums.album_title, songs.song_name, Length song_length
FROM artists
  LEFT JOIN albums ON artists.id = albums.artist_id
  LEFT JOIN songs ON albums.id = songs.album_id;

-- Join the artist and albums table. (Find all albumbs that has letter a  in its name)   
SELECT artists.name, albums.album_title
FROM artists
  LEFT JOIN albums ON artists.id = albums.artist_id
  LEFT JOIN songs ON albums.id = songs.album_id
WHERE album_title like "%a%";


-- Sort the album in  Z-A order (show only the first 4 records.)   

SELECT *
FROM albums
ORDER BY album_title DESC LIMIT 4;

-- Join the albums and song tables

SELECT  albums.album_title
, songs.song_name, FROM artists
   LEFT JOIN albums ON artists.id = albums.artist_id 
   LEFT  JOIN songs ON albums.id = songs.album_id   WHERE ORDER BY album_title DESC  LIMIT 4;